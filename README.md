
                         BOSTON REDEVELOPMENT AUTHORITY REPO

                                 AKASH BANGINWAR

**BRIEF DESCRIPTION**

**MAIN PAGE LAYOUT:**

![webpage.png](https://bitbucket.org/repo/9qLdE5/images/2373018697-webpage.png)
 
The above image represents the basic layout of the Document center. The layout of the existing page has been redesigned to provide the user with optimum user experience and user-friendliness such that the user can achieve his/her goal with minimum number of clicks (making use of 3-click rule). The prime focus of this project is on the functionality and ease of use of Documents center. The exact look may not be replicated from the existing page, however it can be easily enhanced later on. 


Factors taken into consideration while designing,

* 1)	Easy to use, navigate and understand

* 2)	Professionally designed and accessible to modern browsers

* 3)	Responsive and cross platform capability

* 4)	Follows 3-click rule

* 5)	Clear display of information

* 6)	Display of relevant information in single fold

**CENTER PANEL**

This section displays a grid containing document name, its department and last updated date. Also, the gird consists of other functionalities for performing operations on the filtered data.
 
![document part.png](https://bitbucket.org/repo/9qLdE5/images/2118153581-document%20part.png)

**1.	SORTING:**

As shown in the figure, on-click of the glyphicon, the respective column gets sorted. This feature has been provided for certain columns such as ‘Document Name’, ‘Department’ and ‘Date’.

**2.	DOWNLOAD OPTION**

On-click of  icon, the user can download the respective document.

**3.	MAIL OPTION**

On-click of  icon, the document can be e-mailed. It will directly open outlook.

**4.	PRINTING OPTION**

On-click of  icon, the document can be printed.


**5.	OVER VIEW OF INDEX OR CONTENT**

 	On-click of   icon an overview of the document can be viewed.

 ![viewpage.png](https://bitbucket.org/repo/9qLdE5/images/1984685057-viewpage.png)

**6.	PARTIAL INFORMATION LOOKUP**

On-click of   icon, a general summary of the document can be viewed on the bottom right corner.

![info.png](https://bitbucket.org/repo/9qLdE5/images/174977218-info.png)

 

**LEFT PANEL**

This section consists of all the filters required for efficient and easy search.
 
![sidebar.png](https://bitbucket.org/repo/9qLdE5/images/4150218078-sidebar.png)




**7.	FILTER BY DOCUMENT TYPE**

This is a 3-in-1 feature which provides a search box, a combo box as well as a check box to search by document type.
 
**Search:** The user can start typing his desired search field and suggestions will appear.

**Combo box:** The user can select his filter options from the dropdown menu.

**Checkbox:** The user can select multiple options from the dropdown menu to get an optimum result

![filter.png](https://bitbucket.org/repo/9qLdE5/images/218702512-filter.png)
 
**8.	FILTER BY NEIGHBORHOOD**

This section is user to filter by neighborhood. The functionalities are same as in point 
7.

**9.	FILTER BY DEPARTMENT**

This section is user to filter by department. The functionalities are same as in point 7.

**10.	FILTER BY PROGRAM**

This section is user to filter by program. The functionalities are same as in point 7. 

**11.	FILTER BY FROM & TO DATE**

This section provides the user with a date picker which can be used to select the filter dates.

![datefilter.png](https://bitbucket.org/repo/9qLdE5/images/3028552609-datefilter.png)
 


**FUTURE ADVANCEMENTS**

* 1)	Lazy loading: The documents may get loaded, only as and when the user scrolls through the page

* 2)	Dynamic display of information as and when filter parameter is selected

* 3)	Pagination for the grid can be implemented

* 4)	The filter parameters will increase in-order to optimize the search if the user logs in.

**REFFERAL WEBSITES & LIBRARYS** 

* •	http://getbootstrap.com/

* •	http://www.igniteui.com/

* •	http://jqgrid.com/
